<?php

use App\Util\Database\DatabaseConnectionService;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\Tools\Setup;
use PHPUnit\Framework\TestCase;
use Doctrine\ORM\EntityManager;

class DatabaseConnectionServiceTest extends TestCase {
	/**
	 * @var DatabaseConnectionService
	 */
	private $service;
	
	public function setUp() {
		$this->service = new DatabaseConnectionService();
	}
	
	public function testCreateManagerConnection() {
		$databaseMock = $this->createMock(DatabaseConnectionService::class);
		
		$databaseMock->method('onCreate')->willReturn($this->createManager());
		
		$this->assertEquals($databaseMock->onCreate(), $this->service->onCreate());
	}
	
	private function createManager() {
		return EntityManager::create($this->createParameters(), $this->createConfiguration());
	}
	
	private function createParameters(): array {
		return array(
		  'driver'   => DatabaseConnectionService::DATABASE_DRIVER,
		  'user'     => DatabaseConnectionService::DATABASE_USER,
		  'password' => DatabaseConnectionService::DATABASE_PASSWORD,
		  'dbname'   => DatabaseConnectionService::DATABASE_NAME,
		);
	}
	
	private function createConfiguration(): Configuration {
		return Setup::createAnnotationMetadataConfiguration(DatabaseConnectionService::ENTITIES_PATHS, DatabaseConnectionService::DEV_MODE);
	}
}