<?php

namespace App\Model\Context;

use PHPUnit\Framework\TestCase;

class ApplicationContextTest extends TestCase {
	private $context;
	
	public function setUp() {
		$this->context = new ApplicationContext();
		$this->context->addProperty('testKey', 'testValue');
	}
	
	public function testGetProperty() {
		$contextMock = $this->createMock(ApplicationContext::class);
		
		$contextMock->method('getProperty')
		  ->with($this->equalTo('testKey'))
		  ->willReturn('testValue');
		
		$this->assertEquals($contextMock->getProperty('testKey'), $this->context->getProperty('testKey'));
	}
	
	public function testAddPropertyThenPropertyInArray() {
		$this->context->addProperty('testKey', 'testValue');
		$this->assertArrayHasKey('testKey', $this->context->getProperties(), $this->context->getProperty('testKey'));
	}
}