<?php

use App\AppContextFactory;
use App\Model\Context\ApplicationContext;
use PHPUnit\Framework\TestCase;

class AppContextFactoryTest extends TestCase {
	/**
	 * @var AppContextFactory
	 */
	private $factory;
	
	public function setUp() {
		$this->factory = new AppContextFactory(new ApplicationContext());
	}
	
	public function testCreate() {
		$factoryMock = $this->createMock(AppContextFactory::class);
		
		$factoryMock->method('create')->willReturn(new ApplicationContext());
		
		$this->assertEquals($factoryMock->create(), $this->factory->create());
	}
}