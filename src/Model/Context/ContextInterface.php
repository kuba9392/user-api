<?php

namespace App\Model\Context;


interface ContextInterface {
    public function getProperty(string $valueKey);

    public function addProperty(string $valueKey, $propertyValue);
}