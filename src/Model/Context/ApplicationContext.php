<?php

namespace App\Model\Context;

class ApplicationContext implements ContextInterface {
	
	/**
	 * @var array
	 */
	private $properties;
	
	public function addProperty(string $valueKey, $propertyValue) {
		$this->properties[$valueKey] = $propertyValue;
	}
	
	public function getProperty(string $valueKey) {
		return $this->properties[$valueKey];
	}
	
	public function getProperties(): array {
		return $this->properties;
	}
	
	public function registerServices(array $services) {
		foreach($services as $service) {
			$this->addProperty($service::ENTRY, $service);
		}
	}
}