<?php

namespace App;

use App\Model\Context\ApplicationContext;
use App\Util\Database\DatabaseConnectionService;

class AppContextFactory {
	
	public function create() {
		return new ApplicationContext();
	}
	
	private function register() {
		return [
			new DatabaseConnectionService()
		];
	}
}