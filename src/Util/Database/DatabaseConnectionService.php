<?php

namespace App\Util\Database;

use Doctrine\ORM\Configuration;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;

class DatabaseConnectionService {
	
	const DATABASE_USER = "host";
	const DATABASE_DRIVER = "pdo_mysql";
	const DATABASE_PASSWORD = "";
	const DATABASE_NAME = "name";
	
	const ENTITIES_PATHS = ["/src/App/Entity"];
	const DEV_MODE = true;
	
	public function onCreate() {
		return $this->createManager();
	}
	
	private function createManager() {
		return EntityManager::create($this->createParameters(), $this->createConfiguration());
	}
	
	private function createParameters(): array {
		return array(
		  'driver'   => self::DATABASE_DRIVER,
		  'user'     => self::DATABASE_USER,
		  'password' => self::DATABASE_PASSWORD,
		  'dbname'   => self::DATABASE_NAME,
		);
	}
	
	private function createConfiguration(): Configuration {
		return Setup::createAnnotationMetadataConfiguration(self::ENTITIES_PATHS, self::DEV_MODE);
	}
}